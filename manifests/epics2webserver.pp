class role::epics2webserver {
    include profile::genericauth
    include profile::fail2ban
    include profile::weblanserver
    include profile::backupclient
}
