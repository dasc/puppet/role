class role::eeworkstation {
    include profile::ligoorgauth
    include profile::workstation
    include profile::dataanalysis
    include profile::cdsserver
    include profile::eelabuserstation
}
