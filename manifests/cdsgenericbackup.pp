class role::cdsgenericbackup {
    include profile::genericauth
    include profile::backupclient
    include profile::cdsserver
}
