class role::workstation {
    include profile::workstation
    include profile::dataanalysis
    include profile::ligoorgauth
    include profile::cdsserver
}
