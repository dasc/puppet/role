class role::admin {
    include profile::backupclient
    include profile::adminauth
    include profile::cdsserver
}
