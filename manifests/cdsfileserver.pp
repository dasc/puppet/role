class role::cdsfileserver {
    include profile::genericauth
    include profile::cdsserver
    include profile::hafileserver
    include profile::backupclient
}
