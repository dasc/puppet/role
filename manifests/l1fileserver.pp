class role::l1fileserver {
    include profile::genericauth
    include profile::cdsserver
    include profile::hafileserver
    include profile::backup2adminclient
}
