class role::internalwiki {
    include profile::genericauth
    include profile::backupclient
    include profile::cdsserver
    include profile::nscaclient
}
