class role::public2faworkstation {
    include profile::workstation
    include profile::dataanalysis
    include profile::fail2ban
    include profile::twofactorauth
    include profile::cdsserver
}
