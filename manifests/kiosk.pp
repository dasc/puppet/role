class role::kiosk {
    include profile::ligoorgauth
    include profile::kiosk
    include profile::dataanalysis
    include profile::cdsserver
}
