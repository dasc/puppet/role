class role::operatorworkstation {
    include profile::ligoorgauth
    include profile::workstation
    include profile::dataanalysis
    include profile::cdsserver
    include profile::operator
}
