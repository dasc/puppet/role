class role::puppetmaster {
    include profile::genericauth
    include profile::cdsserver
    include profile::backupclient
}
